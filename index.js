import logging from "external:@totates/logging/v1";
import preferences from "external:@totates/preferences/v1";
import EventEmitter from "external:@totates/events/v1";
import throttle from "external:@totates/lodash/v4:throttle";
import distance from "@turf/distance";
import point from "turf-point";

const serviceName = "@totates/geolocation/v1";
const logger = logging.getLogger(serviceName);
logger.setLevel("DEBUG");

const prefs = preferences.getFuncs(serviceName, true);

function getCurrentPosition(options) {
    return new Promise(
        (resolve, reject) => navigator.geolocation.getCurrentPosition(resolve, reject, options)
    );
}

function clone(position) {
    return {
        coords: {
            accuracy: position.coords.accuracy,
            altitude: position.coords.altitude,
            altitudeAccuracy: position.coords.altitudeAccuracy,
            heading: position.coords.heading,
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            speed: position.coords.speed
        },
        timestamp: position.timestamp
    };
}

class PositionGetter extends EventEmitter {
    constructor(
        component, { timeout = 60000, maximumAge = 0, enableHighAccuracy = true }
    ) {
        super();
        this.component = component;
        this.options = { timeout, maximumAge, enableHighAccuracy };
    }

    get() {
        return new Promise((resolve, reject) => {
            const startTime = new Date();
            const timeout = this.options.timeout;
            this.emit("start", startTime, timeout);

            const intervalID = window.setInterval(() => {
                const elapsed = new Date() - startTime;
                if (elapsed < timeout)
                    this.emit("progress", elapsed, timeout);
                else
                    finish(-1, new Error("timeout"));
            }, 1000);

            const finish = (code, result) => {
                window.clearInterval(intervalID);
                this.emit("end", code);
                (code === 0 ? resolve : reject)(result);
            };

            const success = position => {
                try {
                    this._setPosition(position);
                    finish(0, position);
                }
                catch (e) {
                    finish(-2, e);
                }
            };
            // https://developer.mozilla.org/en-US/docs/Web/API/GeolocationPositionError
            const error = err => finish(err.code, err);
            navigator.geolocation.getCurrentPosition(success, error, this.options);
        });
    }
}

class Geolocation extends EventEmitter {
    constructor() {
        super();
        this.savePosition = throttle(
            () => prefs.set("position", this.position).catch(logger.error), 1000
        );
        this.initialized = this._init();
    }

    async _init() {
        this.available = false;
        if ("geolocation" in navigator) {
            const options = { timeout: 1000, maximumAge: 0, enableHighAccuracy: true };
            try {
                var position = await getCurrentPosition(options);
            }
            catch (err) {
                if (err.code === err.TIMEOUT)
                    logger.error("unknown geolocation error", err);
                else
                    logger.debug("geolocation is not available:", err.message);
                return null;
            }
        }
        else {
            logger.debug("geolocation is not available");
            this.position = await prefs.get("position");
            logger.debug("recorded position", this.position);
            return this.position;
        }

        logger.debug("geolocation is available", position);
        this._setPosition(position);
        this.available = true;
        return this.position;

        /* if (navigator.permissions) {
            // https://stackoverflow.com/questions/14862019/
            const status = await navigator.permissions.query({ name: 'geolocation' });
            status.addEventListener('change', () => {});
            if (status.state == 'granted') {
                //allowed
            }
            else if (status.state == 'prompt') {
                // prompt - not yet grated or denied
            }
            else {
                //denied
            }
        }
        const distanceAsset = await this.getAsset('npm-turf:module.distance');
        const module = await import(distanceAsset.url);
        this.computeDistance = module.default; */
    }

    getLonLat() {
        const c = this.position.coords;
        return [c.longitude, c.latitude];
    }

    distanceTo(...lonLat) { /* longitude, latitude */
        return distance(point(this.getLonLat()), point(lonLat));
    }

    moveRandomly(seconds = 1) {
        let [longitude, latitude] = this.getLonLat();
        this.simulateMoveTo(longitude, latitude);
        return setInterval(() => {
            longitude += 0.00001 * (Math.random() - 0.5);
            latitude += 0.00001 * (Math.random() - 0.5);
            this.simulateMoveTo(longitude, latitude);
        }, seconds * 1000);
    }

    simulateMoveTo(longitude, latitude) {
        this.position.timestamp = Date.now();
        const c = this.position.coords;
        c.longitude = longitude;
        c.latitude = latitude;
        this.savePosition();
        this.emit("position", this.position);
    }

    _setPosition(position) {
        this.position = clone(position); /* JSON cannot serialize raw position */
        this.savePosition();
        this.emit("position", this.position);
        logger.debug("position", this.getLonLat(), "±", this.position.coords.accuracy);
    }

    isAvailable() {
        return this.available;
    }

    getCurrentPosition(options = {}) {
        return new PositionGetter(this, options);
    }

    static onError(err) {
        switch (err.code) {
            case err.PERMISSION_DENIED:
                logger.warn("user did not share geolocation");
                break;
            case err.POSITION_UNAVAILABLE:
                logger.warn("position is not available");
                break;
            case err.TIMEOUT:
                logger.warn("position timeout");
                break;
            default:
                logger.warn(`unknown error ${err}`);
                break;
        }
        // logger.warn(err.message);
    }

    async refresh() {
        /* get an approximate position as quickly as possible */
        try {
            const position = await getCurrentPosition(
                { timeout: 5000, maximumAge: 60000, enableHighAccuracy: false }
            );
            this._setPosition(position);
        }
        catch (e) {
            logger.error(e);
        }
        /* then take more time to get a more precise location */
        try {
            const position = await getCurrentPosition(
                { timeout: 60000, maximumAge: 0, enableHighAccuracy: true }
            );
            this._setPosition(position);
        }
        catch (e) {
            logger.error(e);
        }
    }

    getPosition() {
        return this.position;
        /* if (this.position)
            return clone(this.position);
        const strPosition = window.localStorage['geolocation:position'];
        return strPosition ? JSON.parse(strPosition) : null;*/
    }

    trackingEnabled() {
        return !!this.watchID;
    }

    enableTracking(enable) {
        if (enable && !this.watchID)
            this.start();
        else if (!enable && this.watchID)
            this.stop();
    }

    start() {
        if (this.watchID) {
            logger.warn("tracking is already active");
            return;
        }

        const error = Geolocation.onError;
        const success = this._setPosition.bind(this);
        const options = { enableHighAccuracy: true, maximumAge: 1000 };
        this.watchID = navigator.geolocation.watchPosition(success, error, options);
        this.emit("tracking", true);
    }

    stop() {
        if (!this.watchID) {
            logger.warn("tracking is not active");
            return;
        }
        navigator.geolocation.clearWatch(this.watchID);
        this.watchID = null;
        this.emit("tracking", false);
    }

    async cleanup() {
        if (this.watchID)
            this.stop();
    }
}

const geolocation = new Geolocation();
export default geolocation;
